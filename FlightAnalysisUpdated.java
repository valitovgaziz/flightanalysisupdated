import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class FlightAnalysisUpdated {
    public static void main(String[] args) {

        String filePath = "tickets.json";
        try {
            JSONObject jsonObject = readJsonFile(filePath);
            JSONArray ticketsArray = (JSONArray) jsonObject.get("tickets");
            processTickets(ticketsArray);
        } catch (Exception e) {
            System.err.println("Error processing file: " + e.getMessage());
            e.printStackTrace();
        }
    }

    // Function to read JSON file and parse it into JSONObject
    private static JSONObject readJsonFile(String filePath) throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader(filePath);
        return (JSONObject) jsonParser.parse(reader);
    }

    // Function to process tickets to find required information
    private static void processTickets(JSONArray ticketsArray) throws Exception {
        List<Ticket> tickets = new ArrayList<>();
        List<Double> allPrices = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy HH:mm");

        for (Object o : ticketsArray) {
            JSONObject ticket = (JSONObject) o;
            String origin = (String) ticket.get("origin_name");
            String destination = (String) ticket.get("destination_name");
            if (!origin.equals("Владивосток") || !destination.equals("Тель-Авив")) {
                continue;
            }

            String carrier = (String) ticket.get("carrier");
            String departure = ticket.get("departure_date") + " " + ticket.get("departure_time");
            String arrival = ticket.get("arrival_date") + " " + ticket.get("arrival_time");
            long duration = TimeUnit.MILLISECONDS.toMinutes(dateFormat.parse(arrival).getTime() - dateFormat.parse(departure).getTime());
            long price = (long) ticket.get("price");

            tickets.add(new Ticket(carrier, duration, price));
            allPrices.add((double) price);
        }

        Map<String, Long> minDurations = new HashMap<>();
        Map<String, List<Double>> carrierPrices = new HashMap<>();

        for (Ticket ticket : tickets) {
            minDurations.compute(ticket.carrier,
                (k, v) -> (v == null || ticket.duration < v) ? ticket.duration : v);

            carrierPrices.computeIfAbsent(ticket.carrier, k -> new ArrayList<>()).add((double) ticket.price);
        }

        System.out.println("Minimum flight durations by carrier (Vladivostok to Tel-Aviv):");
        minDurations.forEach((carrier, duration) ->
            System.out.println(carrier + ": " + duration + " minutes"));

        System.out.println("\nDifference between average price and median price for all flight:");
        if (!allPrices.isEmpty()) {
            Collections.sort(allPrices);
            double median = (allPrices.size() % 2 != 0) ? allPrices.get(allPrices.size() / 2) : 
                ((allPrices.get(allPrices.size() / 2 - 1) + allPrices.get(allPrices.size() / 2)) / 2.0);
            double average = allPrices.stream().mapToDouble(Double::doubleValue).average().orElse(0);
            double difference = Math.abs(average - median);
            
            System.out.println("Average price: " + average + " rubles");
            System.out.println("Median price: " + median + " rubles");
            System.out.println("Difference between average and median price: " + difference + " rubles");
        } else {
            System.out.println("No tickets found for this route.");
        }
        
    }

    private static class Ticket {
        String carrier;
        long duration;
        double price;

        public Ticket(String carrier, long duration, double price) {
            this.carrier = carrier;
            this.duration = duration;
            this.price = price;
        }
    }
}